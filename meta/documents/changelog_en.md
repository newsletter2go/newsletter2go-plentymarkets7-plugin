 Release Notes for Newsletter2Go
 
## v1.0.0 (2018-03-02) 
- Initial Release of the Plugin
- based on REST-API
- supports Customer-Synchronization and 1-Click-Products
- added language-Selection for 1-Click-Products 
- added easy build-in Conversion-Tracking