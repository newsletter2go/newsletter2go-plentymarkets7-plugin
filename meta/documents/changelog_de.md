 Release Notes for Newsletter2Go
 
## v1.0.0 (2018-03-02) 
- Initiales Release des Plugins
- REST-API-basiert
- unterstützt  Empfänger-Synchronisation und 1-Klick-Produktübernahme
- neue Sprachauswahl für 1-Klick-Produktübernahme
- enthält eingebautes und einfach aktivierbares Conversion-Tracking